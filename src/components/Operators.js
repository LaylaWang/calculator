import React, { Component } from 'react';
import Button from './Button.js';

class Operators extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(operator) {
    this.props.onClick(operator);
  }

  render() {
    return (
      <div>
        <Button buttonText="+" onClick={() => this.handleClick('+')} />
        <Button buttonText="x" onClick={() => this.handleClick('*')} />
        <Button buttonText="-" onClick={() => this.handleClick('-')} />
        <Button buttonText="/" onClick={() => this.handleClick('/')} />
      </div>
    )
  }
}

export default Operators;